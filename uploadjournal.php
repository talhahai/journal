<?php

if (!isset($_SESSION))
	session_start();

if (!isset($_SESSION['userid']) || !isset($_SESSION['token']) || !isset($_SESSION['type']))
{
	header('location: index.php');
}

if ($_SESSION['type'] != 'admin')
{
	header('location: index.php');
}

require_once 'cfg.php';

$Response=file_get_contents($BaseUrl.'/getArticlesList?token='.$_SESSION['token']);
$Response = json_decode($Response,true);

?>
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta http-equiv="x-ua-compatible" content="ie=edge">

	<title>Journal Website Design</title>

	<link rel="stylesheet" href="css/font-awesome.min.css">
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<link href="css/mdb.min.css" rel="stylesheet">
	<link href="css/style.css" rel="stylesheet">
	<style>
	@media (max-width: 740px) {
		.full-height,
		.full-height body,
		.full-height header,
		.full-height header .view {
			height: 700px; 
		}
	}
</style>
</head>
<body class="university">	
	<header>
		<?php include 'nav.php'; ?>

		<div id="home" class="view hm-black-strong-1 jarallax" data-jarallax='{"speed": 0.2}' style="background-image: url('images/bg-upload.jpg'); height: 525px; min-height: 500px">
			<div class="full-bg-img" style="height: 525px">
				<div class="container flex-center">
					<div class="row smooth-scroll">
						<div class="col-md-12 white-text text-center">
							<div class="wow1 fadeInDown" data-wow1-delay="0.2s">
								<h2 class="display-3 font-bold mb-2">UBIT Journal</h2>
								<hr class="hr-light">
								<h3 class="subtext-header mt-4 mb-5">Upload Journal</h3>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<?php include_once 'login_register_modal.php' ?>
	</header>

	<main class="grey lighten-3">
		<div class="container py-5">
			<div class="divider-new mb-0 mt-3 pb-3">
				<h2 class="text-center font-up font-bold wow1 fadeIn">Upload Journal</h2>
			</div>
			<div class="container">
				<div class="card">
					<div class="card-body px-5 pt-5">
						<form id="upload-journal-form" class="form-horizontal" novalidate>
							<div class="md-form">
								<input type="text" id="name" name="name" class="form-control" required>
								<label for="name"><b>Journal Title</b></label>
							</div>

							<div class="md-form pb-1">
								<label class="mb-0"><small><b>Date</b></small></label>
							</div>
							<div class="md-form">
								<input type="date" id="date" name="date" class="form-control" placeholder="Date article" value="<?php echo date('Y-m-d') ?>" required>
							</div>
							<div class="md-form">
								<textarea id="abstract" name="Abstract" rows="5" style="height: auto" class="md-textarea" required></textarea>
								<label for="abstract"><b>Journal Abstract</b></label>
							</div>
							<div class="md-form pb-3">
								<label class="mb-0"><small><b>Cover Picture</b></small></label>
							</div>
							<div class="md-form">
								<input type="file" id="Filedata" name="Filedata">
							</div>
							<div class="table">
								<table class="table">
									<thead class="mdb-color darken-3">
										<tr class="text-white">
											<th colspan="5" style="font-size: 1.25rem" class="text-center">Approved Articles</th>
										</tr>
										<tr class="text-white">
											<th>Publish</th>
											<th>Date</th>
											<th>Title</th>
											<th>Username</th>
										</tr>
									</thead>
									<tbody>
										<?php
										if ($Response['responseHeaderDto']['statusCode'] == 0)
										{
											foreach ($Response['articleDtos'] as $Key => $Article)
											{
												if ($Article['status'] != 1)
													continue;

												?>
												<tr>
													<td><input type="checkbox"" name="articleIds" value="<?php echo $Article['id'] ?>"></td>
													<td><?php echo date('d M, Y @ h:i A', strtotime($Article['createdDate'])) ?></td>
													<td><?php echo $Article['title'] ?></td>
													<td><?php echo $Article['user']['username'] ?></td>
												</tr>
												<?php
											}
										}
										?>
									</tbody>
								</table>

								<div class="text-center">
									<button type="submit" id="btn-submit-upload-journal-form" class="btn btn-lg btn-default active btn-rounded z-depth-1a"><i class="fa fa-send mr-2" aria-hidden="true"></i> Upload Journal</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</main>

	<?php include 'footer.php'; ?>
	<script type="text/javascript" src="js/jquery.min.js"></script>
	<script type="text/javascript" src="js/popper.min.js"></script>
	<script type="text/javascript" src="js/bootstrap.min.js"></script>
	<script type="text/javascript" src="js/mdb.min.js"></script>
	<script type="text/javascript" src="js/scripts.js"></script>
	<script type="text/javascript">
		$('.dropdown-toggle').dropdown();
	</script>
</body>
</html>