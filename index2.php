<!DOCTYPE html>
<html lang="en" class="full-height">

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta http-equiv="x-ua-compatible" content="ie=edge">

	<title>Journal Website Design</title>

	<link rel="stylesheet" href="css/font-awesome.min.css">
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<link href="css/mdb.min.css" rel="stylesheet">
	<link href="css/style.css" rel="stylesheet">
	<style>
	@media (max-width: 740px) {
		.full-height,
		.full-height body,
		.full-height header,
		.full-height header .view {
			height: 700px; 
		} 
	}
</style>
</head>
<body class="university-lp">
	<header>
		<?php include 'nav.php'; ?>

		<div id="home" class="view hm-black-strong-1 jarallax" data-jarallax='{"speed": 0.2}' style="background-image: url('images/ubit.jpg');">
			<div class="full-bg-img">
				<div class="container flex-center">
					<div class="row smooth-scroll">
						<div class="col-md-12 white-text text-center">
							<div class="wow1 fadeInDown" data-wow1-delay="0.2s">
								<h2 class="display-3 font-bold mb-2">UBIT Journal</h2>
								<hr class="hr-light w-50">
								<h3 class="subtext-header mt-4 mb-5">Lorem ipsum dolor sit amet, consectetur adipisicing elit deleniti consequuntur.</h3>
							</div>
							<a class="btn btn-lg btn-outline-white wow1 fadeInUp waves-effect waves-light" data-wow1-delay="0.2s" data-toggle="modal" data-target="#modal-reservation" style="visibility: visible; animation-delay: 0.2s; animation-name: fadeInUp;">
								<i class="fa fa-calendar"></i>
								<span>Make a reservation</span>
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>

		<?php include_once 'login_modal_register.php' ?>

	</header>

	<main>
		<div class="container-fluid indigo lighten-5">
			<div class="container py-5">
				<section id="about" class="section">
					<div class="divider-new mb-0">
						<h2 class="text-center font-up font-bold wow1 fadeIn">ABOUT US</h2>
					</div>
					<p class="text-center font-up pb-3 font-bold wow1 fadeIn" data-wow1-delay="0.2s" style="visibility: visible; animation-delay: 0.2s; animation-name: fadeIn;">WHO WE ARE</p>
					<div class="row wow1 fadeIn" data-wow1-delay="0.2s">
						<div class="col-lg-3 mb-2">
							<div class="view hm-brown-strong"> <img src="images/img.jpg" class="img-fluid z-depth-1" alt="">
								<div class="mask flex-center smooth-scroll">
									<a class="btn btn-outline-white btn-lgs wow1 fadeInUp" data-wow1-delay="0.2s" href="#services" data-offset="100"> <span>GET MORE INFORMATION</span> </a>
								</div>
							</div>
						</div>
						<div class="col-lg-8 offset-lg-1">
							<h5 class="font-bold mb-3">We are a group of legal professionals with experience in seventeen areas of law.</h5>
							<p align="justify"><span>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nemo animi soluta ratione quisquam, dicta ab cupiditate iure eaque? Repellendus voluptatum, magni impedit eaque delectus, beatae maxime temporibus maiores quibusdam quasi. </span><span>Rem magnam ad perferendis iusto sint tempora ea voluptatibus iure, animi excepturi modi aut possimus in hic molestias repellendus illo ullam odit quia velit. Qui expedita sit quo, maxime molestiae. Lorem ipsum dolor sit amet.</span></p>
						</div>
					</div>
				</section>
			</div>
		</div>

		<div class="container py-5">
			<div class="divider-new mb-0 mt-3">
				<h2 class="text-center font-up font-bold wow1 fadeIn">LATEST Editions</h2>
			</div>
			<p class="text-center font-up pb-3 font-bold wow1 fadeIn" data-wow1-delay="0.2s" style="visibility: visible; animation-delay: 0.2s; animation-name: fadeIn;">WHO WE ARE</p>
			<div class="mt-3">
				<div class="row pb-2 pt-3 wow1 fadeIn" data-wow1-delay="0.4s">
					<div class="col-3 col-sm-4 col-md-3">
						<div class="view overlay hm-white-slight">
							<img src="images/journal.jpg" class="img-fluid z-depth-2" alt="Post Image">
							<div class="mask"></div>
						</div>
					</div>
					<div class="col-9 col-sm-8 col-md-9">
						<h6 class="font-up mb-3"><a href="#" class="text-muted grey-text font-bold">| 23 MAY 2017</a></h6>
						<h4><a href="#" class="teal-text pb-3">This is title of the Edition </a></h4>
						<p class="grey-text-3 font-thin mb-0"><b>Published by:</b> Some test name</p>
						<p class="grey-text-3 font-thin"><b>Pages:</b> 500</p>
						<div class="hidden-sm-down">
							<h5 class="teal-text">Abstract</h5>
							<p class="grey-text-3 font-thin small" align="justify">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident.</p>
							<p class="grey-text-3 font-thin small" align="justify">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident.</p>
						</div>
					</div>
					<div class="col-12 mt-3 hidden-md-up">
						<h5 class="teal-text">Abstract</h5>
						<p class="grey-text-3 font-thin small" align="justify">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident.</p>
						<p class="grey-text-3 font-thin small" align="justify">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident.</p>
					</div>
				</div>
				<hr class="between-sections my-2">
				<div class="row pb-2 pt-3 wow1 fadeIn" data-wow1-delay="0.4s">
					<div class="col-3 col-sm-4 col-md-3">
						<div class="view overlay hm-white-slight">
							<img src="images/journal.jpg" class="img-fluid z-depth-2" alt="Post Image">
							<div class="mask"></div>
						</div>
					</div>
					<div class="col-9 col-sm-8 col-md-9">
						<h6 class="font-up mb-3"><a href="#" class="text-muted grey-text font-bold">| 23 MAY 2017</a></h6>
						<h4><a href="#" class="teal-text pb-3">This is title of the Edition </a></h4>
						<p class="grey-text-3 font-thin mb-0"><b>Published by:</b> Some test name</p>
						<p class="grey-text-3 font-thin"><b>Pages:</b> 500</p>
						<div class="hidden-sm-down">
							<h5 class="teal-text">Abstract</h5>
							<p class="grey-text-3 font-thin small" align="justify">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident.</p>
							<p class="grey-text-3 font-thin small" align="justify">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident.</p>
						</div>
					</div>
					<div class="col-12 mt-3 hidden-md-up">
						<h5 class="teal-text">Abstract</h5>
						<p class="grey-text-3 font-thin small" align="justify">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident.</p>
						<p class="grey-text-3 font-thin small" align="justify">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident.</p>
					</div>
				</div>
				<hr class="between-sections my-2">
				<div class="row pb-2 pt-3 wow1 fadeIn" data-wow1-delay="0.4s">
					<div class="col-3 col-sm-4 col-md-3">
						<div class="view overlay hm-white-slight">
							<img src="images/journal.jpg" class="img-fluid z-depth-2" alt="Post Image">
							<div class="mask"></div>
						</div>
					</div>
					<div class="col-9 col-sm-8 col-md-9">
						<h6 class="font-up mb-3"><a href="#" class="text-muted grey-text font-bold">| 23 MAY 2017</a></h6>
						<h4><a href="#" class="teal-text pb-3">This is title of the Edition </a></h4>
						<p class="grey-text-3 font-thin mb-0"><b>Published by:</b> Some test name</p>
						<p class="grey-text-3 font-thin"><b>Pages:</b> 500</p>
						<div class="hidden-sm-down">
							<h5 class="teal-text">Abstract</h5>
							<p class="grey-text-3 font-thin small" align="justify">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident.</p>
							<p class="grey-text-3 font-thin small" align="justify">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident.</p>
						</div>
					</div>
					<div class="col-12 mt-3 hidden-md-up">
						<h5 class="teal-text">Abstract</h5>
						<p class="grey-text-3 font-thin small" align="justify">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident.</p>
						<p class="grey-text-3 font-thin small" align="justify">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident.</p>
					</div>
				</div>
			</div>
		</div>

		<div class="streak streak-photo streak-sm hm-indigo-light" style="background-image: url('images/bg.jpg');">
			<div class="flex-center mask pattern-1">
				<div class="text-center white-text">
					<h2 class="h2-responsive mb-5"><i class="fa fa-quote-left" aria-hidden="true"></i> Creativity requires the courage to let go of certainties <i class="fa fa-quote-right" aria-hidden="true"></i></h2>
					<h5 class="text-center font-italic " data-wow1-delay="0.2s">~ Erich Fromm</h5>
				</div>
			</div>
		</div>

		<div class="container">
			<section id="courses" class="section py-5">
				<div class="divider-new mb-5">
					<h2 class="text-center font-up font-bold wow1 fadeIn">SUBJECTS</h2>
				</div>
				<div class="row mb-3 wow1 fadeIn" data-wow1-delay="0.4s">
					<div class="col-md-2 mb-5 flex-center">
						<img src="images/subj1.jpg" class="img-fluid z-depth-1 rounded-circle">
					</div>
					<div class="col-md-4 text-center text-md-left mb-3">
						<h4>Website</h4>
						<p class="grey-text mt-4">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quas ea animi quae quisquam dolor sit totam iure provident qui consequatur! Dolorum velit esse cum odit.</p>
					</div>
					<div class="col-md-2 mb-5 flex-center">
						<img src="images/subj2.jpg" class="img-fluid  z-depth-1 rounded-circle">
					</div>
					<div class="col-md-4 text-center text-md-left mb-3">
						<h4>Android</h4>
						<p class="grey-text mt-4">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quas ea animi quae quisquam dolor sit totam iure provident qui consequatur! Dolorum velit esse cum odit.</p>
					</div>
				</div>
				<div class="row mb-3 wow1 fadeIn" data-wow1-delay="0.4s">
					<div class="col-md-2 mb-5 flex-center">
						<img src="images/subj3.jpg" class="img-fluid z-depth-1 rounded-circle">
					</div>
					<div class="col-md-4 text-center text-md-left mb-3">
						<h4>iOS</h4>
						<p class="grey-text mt-4">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quas ea animi quae quisquam dolor sit totam iure provident qui consequatur! Dolorum velit esse cum odit.</p>
					</div>
					<div class="col-md-2 mb-5 flex-center">
						<img src="images/subj4.jpg" class="img-fluid z-depth-1 rounded-circle">
					</div>
					<div class="col-md-4 text-center text-md-left mb-3">
						<h4>Desktop</h4>
						<p class="grey-text mt-4">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quas ea animi quae quisquam dolor sit totam iure provident qui consequatur! Dolorum velit esse cum odit.</p>
					</div>
				</div>
				<div class="row wow1 fadeIn" data-wow1-delay="0.4s">
					<div class="col-md-2 mb-5 flex-center">
						<img src="images/subj5.jpg" class="img-fluid z-depth-1 rounded-circle">
					</div>
					<div class="col-md-4 text-center text-md-left mb-3">
						<h4>Artificial Intelligence</h4>
						<p class="grey-text mt-4">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quas ea animi quae quisquam dolor sit totam iure provident qui consequatur! Dolorum velit esse cum odit.</p>
					</div>
					<div class="col-md-2 mb-5 flex-center">
						<img src="images/subj6.jpg" class="img-fluid z-depth-1 rounded-circle">
					</div>
					<div class="col-md-4 text-center text-md-left mb-3">
						<h4>Thesis</h4>
						<p class="grey-text mt-4">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quas ea animi quae quisquam dolor sit totam iure provident qui consequatur! Dolorum velit esse cum odit.</p>
					</div>
				</div>
			</section>
		</div>

		<div class="streak streak-photo streak-long-2 hm-black-strong" style="background-image:url('images/bg1.jpg')">
			<div class="mask flex-center">
				<div class="container">
					<h3 class="text-center text-white mb-5 font-up font-bold wow1 fadeIn" data-wow1-delay="0.2s">Great people trusted our services</h3>
					<div class="row text-white text-center wow1 fadeIn" data-wow1-delay="0.2s">
						<div class="col-md-3 mb-2">
							<h1 class="green-text mb-1 font-bold">+950</h1>
							<p>Happy clients</p>
						</div>
						<div class="col-md-3 mb-2">
							<h1 class="green-text mb-1 font-bold">+150</h1>
							<p>Projects completed</p>
						</div>
						<div class="col-md-3 mb-2">
							<h1 class="green-text mb-1 font-bold">+85</h1>
							<p>Winning awards</p>
						</div>
						<div class="col-md-3">
							<h1 class="green-text mb-1 font-bold">+246</h1>
							<p>Cups of coffee</p>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="container-fluid background-grey">
			<div class="container">
				<section id="about" class="section py-5">
					<div class="divider-new mb-0">
						<h2 class="text-center font-up font-bold wow1 fadeIn">PROCESS</h2>
					</div>
					<p class="text-center font-up pb-3 font-bold wow1 fadeIn" data-wow1-delay="0.2s" style="visibility: visible; animation-delay: 0.2s; animation-name: fadeIn;">WHO WE ARE</p>
					<div class="tabs-wrapper wow1 fadeIn" data-wow1-delay="0.4s"> 
						<ul class="nav classic-tabs tabs-grey mdb-color d-flex justify-content-center" role="tablist">
							<li class="nav-item">
								<a class="nav-link waves-light active" data-toggle="tab" href="#panel51" role="tab">Coming from the train station</a>
							</li>
							<li class="nav-item">
								<a class="nav-link waves-light" data-toggle="tab" href="#panel52" role="tab">Coming from the airport</a>
							</li>
							<li class="nav-item">
								<a class="nav-link waves-light" data-toggle="tab" href="#panel53" role="tab">Coming from bus station</a>
							</li>
						</ul>
					</div>

					<div class="tab-content card wow1 fadeIn" data-wow1-delay="0.4s">
						<div class="tab-pane fade in show active" id="panel51" role="tabpanel">
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nihil odit magnam minima, soluta doloribus reiciendis molestiae placeat unde eos molestias. Quisquam aperiam, pariatur. Tempora, placeat ratione porro voluptate odit minima.</p>
						</div>
						<div class="tab-pane fade" id="panel52" role="tabpanel">
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nihil odit magnam minima, soluta doloribus reiciendis molestiae placeat unde eos molestias. Quisquam aperiam, pariatur. Tempora, placeat ratione porro voluptate odit minima.</p>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nihil odit magnam minima, soluta doloribus reiciendis molestiae placeat unde eos molestias. Quisquam aperiam, pariatur. Tempora, placeat ratione porro voluptate odit minima.</p>
						</div>
						<div class="tab-pane fade" id="panel53" role="tabpanel">
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nihil odit magnam minima, soluta doloribus reiciendis molestiae placeat unde eos molestias. Quisquam aperiam, pariatur. Tempora, placeat ratione porro voluptate odit minima.</p>
						</div>
					</div>
				</section>
			</div>
		</div>
	</main>

	<?php include 'footer.php'; ?>
	<script type="text/javascript" src="js/jquery.min.js"></script>
	<script type="text/javascript" src="js/popper.min.js"></script>
	<script type="text/javascript" src="js/bootstrap.min.js"></script>
	<script type="text/javascript" src="js/mdb.min.js"></script>
	<script>
		new wow1().init();
		$(document).ready(function() {
			$('.mdb-select').material_select();
		});
	</script>
</body>
</html>