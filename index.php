<?php

if (!isset($_SESSION))
	session_start();

require_once 'cfg.php';

$Response=file_get_contents($BaseUrl.'/getRecentJournals');
$Response = json_decode($Response,true);

?>
<!DOCTYPE html>
<html lang="en" class="full-height">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta http-equiv="x-ua-compatible" content="ie=edge">

	<title>Journal Website Design</title>

	<link rel="stylesheet" href="css/font-awesome.min.css">
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<link href="css/mdb.min.css" rel="stylesheet">
	<link href="css/style.css" rel="stylesheet">
	<style>
	@media (max-width: 740px) {
		.full-height,
		.full-height body,
		.full-height header,
		.full-height header .view {
			height: 700px; 
		} 
	}

	.steps-form-2 {
		display: table;
		width: 100%;
		position: relative; 
	}
	.steps-form-2 .steps-row-2 {
		display: table-row; 
	}
	.steps-form-2 .steps-row-2:before {
		top: 35px;
		left: 50px;
		bottom: 0;
		position: absolute;
		content: " ";
		width: calc(100% - 100px);
		height: 2px;
		background-color: #7283a7;
	}
	.steps-form-2 .steps-row-2 .steps-step-2 {
		display: table-cell;
		text-align: center;
		position: relative;
	}
	.steps-form-2 .steps-row-2 .steps-step-2 p {
		margin-top: 0.5rem; 
	}
	.steps-form-2 .steps-row-2 .steps-step-2 button[disabled] {
		opacity: 1 !important;
		filter: alpha(opacity=100) !important; 
	}
	.steps-form-2 .steps-row-2 .steps-step-2 .btn-circle-2 {
		width: 70px;
		height: 70px;
		border: 2px solid #59698D;
		background-color: white !important;
		color: #59698D !important;
		border-radius: 50%;
		padding: 22px 18px 15px 18px;
		margin-top: -22px; 
	}
	.steps-form-2 .steps-row-2 .steps-step-2 .btn-circle-2:hover {
		border: 2px solid #4285F4;
		color: #4285F4 !important;
		background-color: white !important; 
	}
	.steps-form-2 .steps-row-2 .steps-step-2 .btn-circle-2 .fa {
		font-size: 1.7rem;
	}
</style>
</head>
<body class="university">
	<header>
		<?php include 'nav.php'; ?>
		<section class="view" id="home">
			<div id="home" class="view hm-black-strong-1 jarallax" data-jarallax='{"speed": 0.2}' style="background-image: url('images/ubit.jpg');">
				<div class="full-bg-img">
					<div id="particles" class="particles"></div>
					<div id="particles-c" class="particles particles-c"></div>
					<div class="container flex-center">
						<ul>
							<li>
								<div class="row smooth-scroll">
									<div class="col-md-12 white-text text-center">
										<div class="wow fadeInDown" data-wow-delay="0.2s">
											<h2 class="display-3 font-bold mb-2">UBIT Journal</h2>
											<hr class="hr-light w-50">
											<h3 class="subtext-header mt-4 mb-5">Lorem ipsum dolor sit amet, consectetur adipisicing elit deleniti consequuntur.</h3>
										</div>
										<a class="btn btn-lg active wow fadeInUp waves-effect waves-light" data-toggle="modal" data-target="#login-modal" data-wow-delay="0.2s" data-toggle="modal" data-target="#modal-reservation" style="visibility: visible; animation-delay: 0.2s; animation-name: fadeInUp; background-color: #1d4870">
											<span>JOIN US</span>
										</a>
										<a class="btn btn-lg btn-default active wow fadeInUp waves-effect waves-light" data-toggle="modal" data-target="#login-modal" data-wow-delay="0.2s" data-toggle="modal" data-target="#modal-reservation" style="visibility: visible; animation-delay: 0.2s; animation-name: fadeInUp;">
											<span>LOGIN</span>
										</a>
									</div>
								</div>
							</li>
							<li class="mt-5">
								<div id="carouselExampleIndicators" data-interval="3000" class="carousel slide wow fadeInDown text-white" data-ride="carousel">
									<ol class="carousel-indicators hidden-xs-up">
										<li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
										<li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
										<li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
										<li data-target="#carouselExampleIndicators" data-slide-to="3"></li>
										<li data-target="#carouselExampleIndicators" data-slide-to="4"></li>
										<li data-target="#carouselExampleIndicators" data-slide-to="5"></li>
									</ol>
									<div class="carousel-inner" role="listbox">
										<div class="carousel-item active">
											<ul>
												<li>
													<h1 class="h1-responsive wow fadeInDown uppercase" data-wow-delay="0.2s">Tracking Transactions</h1>
												</li>
												<li><br><br></li>
											</ul>
										</div>
										<div class="carousel-item">
											<ul>
												<li>
													<h1 class="h1-responsive wow fadeInDown uppercase" data-wow-delay="0.2s">Scheduling Transactions</h1>
												</li>
												<li><br><br></li>
											</ul>
										</div>
										<div class="carousel-item">
											<ul>
												<li>
													<h1 class="h1-responsive wow fadeInDown uppercase" data-wow-delay="0.2s">Organizing bills and receipts</h1>
												</li>
												<li><br><br></li>
											</ul>
										</div>
										<div class="carousel-item">
											<ul>
												<li>
													<h1 class="h1-responsive wow fadeInDown uppercase" data-wow-delay="0.2s">Payment alerts</h1>
												</li>
												<li><br><br></li>
											</ul>
										</div>
										<div class="carousel-item">
											<ul>
												<li>
													<h1 class="h1-responsive wow fadeInDown uppercase" data-wow-delay="0.2s">Strong Search</h1>
												</li>
												<li><br><br></li>
											</ul>
										</div>
										<div class="carousel-item">
											<ul>
												<li>
													<h1 class="h1-responsive wow fadeInDown uppercase" data-wow-delay="0.2s">Reportings</h1>
												</li>
												<li><br><br></li>
											</ul>
										</div>
									</div>
								</div>
							</li>
						</ul>						
					</div>
				</div>
			</div>
		</section>

		<?php include_once 'login_register_modal.php' ?>

	</header>

	<main>
		<section id="about" class="section">
			<div class="container-fluid">
				<div class="container py-5">
					<div class="divider-new mb-0">
						<h2 class="text-center font-up font-bold wow fadeIn">ABOUT US</h2>
					</div>
					<p class="text-center font-up pb-3 font-bold wow fadeIn" data-wow-delay="0.2s" style="visibility: visible; animation-delay: 0.2s; animation-name: fadeIn;">WHO WE ARE</p>
					<div class="row wow fadeIn" data-wow-delay="0.2s">
						<div class="col-lg-3 col-md-4 mb-2">
							<div class="view hm-brown-strong"> <img src="images/img.jpg" class="img-fluid z-depth-1" style="max-height: 200px;">
								<div class="mask flex-center smooth-scroll">
									<a class="btn btn-outline-white btn-lgs wow fadeInUp" data-wow-delay="0.2s" href="#services" data-offset="100"> <span>GET MORE INFORMATION</span> </a>
								</div>
							</div>
						</div>
						<div class="col-lg-8 offset-lg-1 col-md-7 offset-md-1">
							<h5 class="font-bold mb-3">We are a group of legal professionals with experience in seventeen areas of law.</h5>
							<p align="justify"><span>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nemo animi soluta ratione quisquam, dicta ab cupiditate iure eaque? Repellendus voluptatum, magni impedit eaque delectus, beatae maxime temporibus maiores quibusdam quasi. </span><span>Rem magnam ad perferendis iusto sint tempora ea voluptatibus iure, animi excepturi modi aut possimus in hic molestias repellendus illo ullam odit quia velit. Qui expedita sit quo, maxime molestiae. Lorem ipsum dolor sit amet.</span></p>
						</div>
					</div>
				</div>
			</div>
		</section>

		<section id="latest" class="section grey lighten-3" style="border-top: 2px solid #b0bec5">
			<div class="container py-5">
				<div class="divider-new mb-0">
					<h2 class="text-center font-up font-bold wow fadeIn">LATEST EDITIONS</h2>
				</div>
				<p class="text-center font-up pb-3 font-bold wow fadeIn" data-wow-delay="0.2s" style="visibility: visible; animation-delay: 0.2s; animation-name: fadeIn;">WHO WE ARE</p>
				<div class="mt-3">
					<?php
					if ($Response['responseHeaderDto']['statusCode'] == 0)
					{
						foreach ($Response['journalDtos'] as $Key => $Journal)
						{
							if ($Key != 0)
								echo '<hr class="between-sections my-2">';

							if ($Key == 3)
								break;

							?>
							
							<div class="row pb-2 pt-3 wow fadeIn" data-wow-delay="0.4s">
								<div class="col-3 col-sm-4 col-md-3">
									<a href="edition.php?id=<?php echo $Journal['id'] ?>" role="button">
										<img src="<?php echo $Journal['image']['url'] ?>" class="img-fluid z-depth-2" alt="<?php echo $Journal['name'] ?>">
									</a>
								</div>
								<div class="col-9 col-sm-8 col-md-9">
									<h6 class="font-up mb-3"><div class="text-muted grey-text font-bold">| <?php echo date('F d, Y', strtotime($Journal['date'])) ?></div></h6>
									<h4><a href="edition.php?id=<?php echo $Journal['id'] ?>" class="teal-text pb-3"><?php echo $Journal['name'] ?></a></h4>
									<p class="grey-text-3 font-thin mb-0"><b>Published by:</b> <?php echo $Journal['publisher'] ?></p>
									<p class="grey-text-3 font-thin"><b>Views:</b> <?php echo $Journal['numberOfViews'] ?></p>
									<div class="hidden-sm-down">
										<h5 class="teal-text">Abstract</h5>
										<p class="grey-text-3 font-thin small" align="justify"><?php echo $Journal['abstract'] ?></p>
									</div>
								</div>
								<div class="col-12 mt-3 hidden-md-up">
									<h5 class="teal-text">Abstract</h5>
									<p class="grey-text-3 font-thin small" align="justify"><?php echo $Journal['abstract'] ?></p>
								</div>
							</div>

							<?php
						}
					}
					?>
				</div>
			</div>
		</section>

		<div class="streak streak-photo streak-sm hm-black-strong" style="background-image: url('images/bg-quote.jpg');">
			<div class="flex-center mask pattern-1">
				<div class="text-center white-text">
					<h2 class="h2-responsive mb-5"><i class="fa fa-quote-left" aria-hidden="true"></i> Creativity requires the courage to let go of certainties <i class="fa fa-quote-right" aria-hidden="true"></i></h2>
					<h5 class="text-center font-italic " data-wow-delay="0.2s">~ Erich Fromm</h5>
				</div>
			</div>
		</div>

		<section id="subjects" class="section">
			<div class="container py-5">
				<div class="divider-new mb-0 pb-3">
					<h2 class="text-center font-up font-bold wow fadeIn">SUBJECTS</h2>
				</div>
				<div class="row my-3 wow fadeIn" data-wow-delay="0.4s">
					<div class="col-md-2 mb-5 flex-center" style="height: auto">
						<img src="images/subjects/oop.jpg" class="img-fluid z-depth-1 rounded-circle" style="max-width: 160px">
					</div>
					<div class="col-md-4 col-12 text-center text-md-left mb-3">
						<h4>Object-Oriented Programming</h4>
						<p class="grey-text mt-4">OOP is a programming paradigm that uses objects, data structures consisting of data fields and methods, and their interactions to design applications and computer programs.</p>
					</div>
					<div class="col-md-2 mb-5 flex-center" style="height: auto">
						<img src="images/subjects/se.png" class="img-fluid  z-depth-1 rounded-circle" style="max-width: 160px">
					</div>
					<div class="col-md-4 text-center text-md-left mb-3">
						<h4>Software Engineering</h4>
						<p class="grey-text mt-4">Software engineering is a branch of computer science that deals with the design, implementation, and maintenance of complex computer programs..</p>
					</div>
				</div>
				<div class="row mt-3 wow fadeIn" data-wow-delay="0.4s">
					<div class="col-md-2 mb-5 flex-center" style="height: auto">
						<img src="images/subjects/ds.png" class="img-fluid z-depth-1 rounded-circle" style="max-width: 160px">
					</div>
					<div class="col-md-4 text-center text-md-left mb-3">
						<h4>Data Structures</h4>
						<p class="grey-text mt-4">Data structure is an organization of data and its storage allocations. A data structure, in the simplest term is a scheme for organizing logically related data in computer's memory. </p>
					</div>
					<div class="col-md-2 mb-5 flex-center" style="height: auto">
						<img src="images/subjects/ai.png" class="img-fluid z-depth-1 rounded-circle" style="max-width: 160px">
					</div>
					<div class="col-md-4 text-center text-md-left mb-3">
						<h4>Artificial Intelligence</h4>
						<p class="grey-text mt-4">The theory and development of computer systems able to perform tasks normally requiring human intelligence, such as visual perception, speech recognition, decision-making.</p>
					</div>
				</div>
				<div class="row wow fadeIn" data-wow-delay="0.4s">
					<div class="col-md-2 mb-5 flex-center" style="height: auto">
						<img src="images/subjects/db.png" class="img-fluid z-depth-1 rounded-circle" style="max-width: 160px">
					</div>
					<div class="col-md-4 text-center text-md-left mb-3">
						<h4>Database</h4>
						<p class="grey-text mt-4">A database is an assortment of data that is organized to be easily accessed, managed and updated, and range from relational databases to cloud databases.</p>
					</div>
					<div class="col-md-2 mb-5 flex-center" style="height: auto">
						<img src="images/subjects/crypto.png" class="img-fluid z-depth-1 rounded-circle" style="max-width: 160px">
					</div>
					<div class="col-md-4 text-center text-md-left mb-3">
						<h4>Cryptography</h4>
						<p class="grey-text mt-4">Discipline or techniques employed in protecting integrity or secrecy of electronic messages by converting them into unreadable (cipher text) form.</p>
					</div>
				</div>
			</div>
		</section>

		<div class="streak streak-photo streak-long-2 hm-black-strong" style="background-image:url('images/stats.jpg')">
			<div class="mask flex-center">
				<div class="container">
					<h3 class="text-center text-white mb-5 font-up font-bold wow fadeIn" data-wow-delay="0.2s">Great people trusted our services</h3>
					<div class="row text-white text-center wow fadeIn" data-wow-delay="0.2s">
						<div class="col-md-3 col-sm-6 mb-2">
							<h1 class="green-text mb-1 font-bold">+950</h1>
							<p>Happy clients</p>
						</div>
						<div class="col-md-3 col-sm-6 mb-2">
							<h1 class="green-text mb-1 font-bold">+150</h1>
							<p>Projects completed</p>
						</div>
						<div class="col-md-3 col-sm-6 mb-2">
							<h1 class="green-text mb-1 font-bold">+85</h1>
							<p>Winning awards</p>
						</div>
						<div class="col-md-3 col-sm-6 mb-2">
							<h1 class="green-text mb-1 font-bold">+246</h1>
							<p>Cups of coffee</p>
						</div>
					</div>
				</div>
			</div>
		</div>

		<section id="process" class="section">
			<div class="container-fluid grey lighten-3">
				<div class="container py-5">
					<div class="divider-new mb-0">
						<h2 class="text-center font-up font-bold wow fadeIn">PROCESS</h2>
					</div>
					<p class="text-center font-up pb-3 font-bold wow fadeIn" data-wow-delay="0.2s" style="visibility: visible; animation-delay: 0.2s; animation-name: fadeIn;">WHO WE ARE</p>

					<div class="card px-4 pt-4">
						<div class="steps-form-2 p-4">
							<div class="steps-row-2 process-panel-2 d-flex justify-content-between">
								<div class="steps-step-2">
									<a data-href="#step-0" type="button" class="btn btn-amber btn-circle-2 waves-effect process-step-1" data-toggle="tooltip" data-placement="top" title="Sign in"><i class="fa fa-sign-in" aria-hidden="true"></i></a>
								</div>
								<div class="steps-step-2">
									<a data-href="#step-2" type="button" class="btn btn-blue-grey btn-circle-2 waves-effect" data-toggle="tooltip" data-placement="top" title="Write Article"><i class="fa fa-pencil" aria-hidden="true"></i></a>
								</div>
								<div class="steps-step-2">
									<a data-href="#step-3" type="button" class="btn btn-blue-grey btn-circle-2 waves-effect" data-toggle="tooltip" data-placement="top" title="Submit Article"><i class="fa fa-send" aria-hidden="true"></i></a>
								</div>
								<div class="steps-step-2">
									<a data-href="#step-4" type="button" class="btn btn-blue-grey btn-circle-2 waves-effect mr-0" data-toggle="tooltip" data-placement="top" title="Finish"><i class="fa fa-check" aria-hidden="true"></i></a>
								</div>
							</div>
						</div>

						<div class="row process-content-2 p-4" id="step-0">
							<div class="col-md-12">
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nihil odit magnam minima, soluta doloribus reiciendis molestiae placeat unde eos molestias. Quisquam aperiam, pariatur. Tempora, placeat ratione porro voluptate odit minima.</p>
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nihil odit magnam minima, soluta doloribus reiciendis molestiae placeat unde eos molestias. Quisquam aperiam, pariatur. Tempora, placeat ratione porro voluptate odit minima.</p>
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nihil odit magnam minima, soluta doloribus reiciendis molestiae placeat unde eos molestias. Quisquam aperiam, pariatur. Tempora, placeat ratione porro voluptate odit minima.</p>
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nihil odit magnam minima, soluta doloribus reiciendis molestiae placeat unde eos molestias. Quisquam aperiam, pariatur. Tempora, placeat ratione porro voluptate odit minima.</p>
								<button class="btn btn-mdb-color btn-rounded nextBtn-2 float-right" type="button">Next</button>
							</div>
						</div>
						<div class="row process-content-2 p-4" id="step-2">
							<div class="col-md-12">
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nihil odit magnam minima, soluta doloribus reiciendis molestiae placeat unde eos molestias. Quisquam aperiam, pariatur. Tempora, placeat ratione porro voluptate odit minima.</p>
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nihil odit magnam minima, soluta doloribus reiciendis molestiae placeat unde eos molestias. Quisquam aperiam, pariatur. Tempora, placeat ratione porro voluptate odit minima.</p>
								<button class="btn btn-mdb-color btn-rounded prevBtn-2 float-left" type="button">Previous</button>
								<button class="btn btn-mdb-color btn-rounded nextBtn-2 float-right" type="button">Next</button>
							</div>
						</div>
						<div class="row process-content-2 p-4" id="step-3">
							<div class="col-md-12">
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nihil odit magnam minima, soluta doloribus reiciendis molestiae placeat unde eos molestias. Quisquam aperiam, pariatur. Tempora, placeat ratione porro voluptate odit minima.</p>
								<button class="btn btn-mdb-color btn-rounded prevBtn-2 float-left" type="button">Previous</button>
								<button class="btn btn-mdb-color btn-rounded nextBtn-2 float-right" type="button">Next</button>
							</div>
						</div>
						<div class="row process-content-2 p-4" id="step-4">
							<div class="col-md-12">
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nihil odit magnam minima, soluta doloribus reiciendis molestiae placeat unde eos molestias. Quisquam aperiam, pariatur. Tempora, placeat ratione porro voluptate odit minima.</p>
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nihil odit magnam minima, soluta doloribus reiciendis molestiae placeat unde eos molestias. Quisquam aperiam, pariatur. Tempora, placeat ratione porro voluptate odit minima.</p>
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nihil odit magnam minima, soluta doloribus reiciendis molestiae placeat unde eos molestias. Quisquam aperiam, pariatur. Tempora, placeat ratione porro voluptate odit minima.</p>
								<button class="btn btn-mdb-color btn-rounded prevBtn-2 float-left" type="button">Previous</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
	</main>

	<?php include 'footer.php'; ?>
	<script type="text/javascript" src="js/jquery.min.js"></script>
	<script type="text/javascript" src="js/popper.min.js"></script>
	<script type="text/javascript" src="js/tether.min.js"></script>
	<script type="text/javascript" src="js/bootstrap.min.js"></script>
	<script type="text/javascript" src="js/mdb.min.js"></script>
	<script type="text/javascript" src="js/jquery.particleground.min.js"></script>
	<script type="text/javascript" src="js/scripts.js"></script>
	<script>
		new WOW().init();
		$(document).ready(function() {
			$(document).ready(function () {
				var navListItems = $('div.process-panel-2 div a'),
				allWells = $('.process-content-2'),
				allNextBtn = $('.nextBtn-2'),
				allPrevBtn = $('.prevBtn-2');

				allWells.hide();

				navListItems.click(function (e) {
					e.preventDefault();
					var $target = $($(this).data('href')),
					$item = $(this);

					if (!$item.hasClass('disabled')) {
						navListItems.removeClass('btn-amber').addClass('btn-blue-grey');
						$item.addClass('btn-amber');
						allWells.hide();
						$target.show();
						$target.find('input:eq(0)').focus();
					}
				});

				allPrevBtn.click(function(){
					var curStep = $(this).closest(".process-content-2"),
					curStepBtn = curStep.attr("id"),
					prevStepSteps = $('div.process-panel-2 div a[href="#' + curStepBtn + '"]').parent().prev().children("a");

					prevStepSteps.removeAttr('disabled').trigger('click');
				});

				allNextBtn.click(function(){
					var curStep = $(this).closest(".process-content-2"),
					curStepBtn = curStep.attr("id"),
					nextStepSteps = $('div.process-panel-2 div a[href="#' + curStepBtn + '"]').parent().next().children("a"),
					curInputs = curStep.find("input[type='text'],input[type='url']"),
					isValid = true;

					$(".form-group").removeClass("has-error");
					for(var i=0; i< curInputs.length; i++){
						if (!curInputs[i].validity.valid){
							isValid = false;
							$(curInputs[i]).closest(".form-group").addClass("has-error");
						}
					}

					if (isValid)
						nextStepSteps.removeAttr('disabled').trigger('click');
				});

				$('div.process-panel-2 div a.btn-amber').trigger('click');
			});

			$('.mdb-select').material_select();

			$("a").on('click', function(event) {
				if (this.hash !== "") {
					var hash = this.hash;
					$('html, body').animate({
						scrollTop: $(hash).offset().top
					}, 1000, function(){      	
						window.location.hash = hash;
					});
				}
			});
		});
		$("#particles").particleground({
			minSpeedX: .6,
			minSpeedY: .6,
			dotColor: "#ffffff",
			lineColor: "#ffffff",
			density: 8e3,
			particleRadius: 3,
			parallaxMultiplier: 5.2,
			proximity: 0
		});
	</script>
</body>
</html>