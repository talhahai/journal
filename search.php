<?php

if (!isset($_SESSION))
	session_start();

require_once 'cfg.php';

$Response=file_get_contents($BaseUrl.'/getRecentJournals');
$Response = json_decode($Response,true);

?>
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta http-equiv="x-ua-compatible" content="ie=edge">

	<title>Journal Website Design</title>

	<link rel="stylesheet" href="css/font-awesome.min.css">
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<link href="css/mdb.min.css" rel="stylesheet">
	<link href="css/style.css" rel="stylesheet">
	<style>
	@media (max-width: 740px) {
		.full-height,
		.full-height body,
		.full-height header,
		.full-height header .view {
			height: 700px; 
		} 
	}
</style>
</head>
<body class="university">
	<header>
		<?php include 'nav.php'; ?>

		<div id="home" class="view hm-black-strong-1 jarallax" data-jarallax='{"speed": 0.2}' style="background-image: url('images/bg-search.jpg'); height: 525px; min-height: 500px">
			<div class="full-bg-img" style="height: 525px">
				<div class="container flex-center">
					<div class="row smooth-scroll">
						<div class="col-md-12 white-text text-center">
							<div class="wow1 fadeInDown" data-wow1-delay="0.2s">
								<h2 class="display-3 font-bold mb-2">UBIT Journal</h2>
								<hr class="hr-light">
								<h3 class="subtext-header mt-4 mb-5">Search Edition</h3>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<?php include_once 'login_register_modal.php' ?>
	</header>

	<main class="grey lighten-3">
		<div class="container py-5">
			<div class="divider-new mb-0 mt-3 pb-3">
				<h2 class="text-center font-up font-bold wow1 fadeIn">SEARCH EDITION</h2>
			</div>

			<div class="card">
				<div class="card-body mx-4">
					<form class="form-inline">
						<div class="col-md-5">
							<div class="md-form form-sm form-group">
								<i class="fa fa-envelope prefix"></i>
								<input type="text" id="title" class="form-control">
								<label for="title">Title</label>
							</div>
						</div>
						<div class="col-md-2">
							<div class="md-form form-sm form-group">
								<a class="btn btn-primary">Search</a>
							</div>
						</div>
					</form>
				</div>
			</div>

			<div class="mt-3">
				<?php
				if ($Response['responseHeaderDto']['statusCode'] == 0)
				{
					foreach ($Response['journalDtos'] as $Key => $Journal)
					{
						if ($Key != 0)
							echo '<hr class="between-sections my-2">';

						?>
						<div class="row pb-2 pt-3">
							<div class="col-3 col-sm-4 col-md-3">
								<a href="edition.php?id=<?php echo $Journal['id'] ?>" role="button">
									<img src="<?php echo $Journal['image']['url'] ?>" class="img-fluid z-depth-2" alt="<?php echo $Journal['name'] ?>">
								</a>
							</div>
							<div class="col-9 col-sm-8 col-md-9">
								<h6 class="font-up mb-3"><div class="text-muted grey-text font-bold">| <?php echo date('F d, Y', strtotime($Journal['date'])) ?></div></h6>
								<h4><a href="edition.php?id=<?php echo $Journal['id'] ?>" class="teal-text pb-3"><?php echo $Journal['name'] ?></a></h4>
								<p class="grey-text-3 font-thin mb-0"><b>Published by:</b> <?php echo $Journal['publisher'] ?></p>
								<p class="grey-text-3 font-thin"><b>Views:</b> <?php echo $Journal['numberOfViews'] ?></p>
								<div class="hidden-sm-down">
									<h5 class="teal-text">Abstract</h5>
									<p class="grey-text-3 font-thin small" align="justify"><?php echo $Journal['abstract'] ?></p>
								</div>
							</div>
							<div class="col-12 mt-3 hidden-md-up">
								<h5 class="teal-text">Abstract</h5>
								<p class="grey-text-3 font-thin small" align="justify"><?php echo $Journal['abstract'] ?></p>
							</div>
						</div>

						<?php
					}
				}
				?>
			</div>
		</div>
	</main>

	<?php include 'footer.php'; ?>
	<script type="text/javascript" src="js/jquery.min.js"></script>
	<script type="text/javascript" src="js/popper.min.js"></script>
	<script type="text/javascript" src="js/bootstrap.min.js"></script>
	<script type="text/javascript" src="js/mdb.min.js"></script>
	<script type="text/javascript" src="js/scripts.js"></script>
</body>
</html>