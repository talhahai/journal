<div class="modal fade" id="login-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog cascading-modal" role="document">
		<div class="modal-content">
			<div class="modal-c-tabs">
				<ul class="nav nav-tabs tabs-2 light-blue darken-3" role="tablist">
					<li class="nav-item">
						<a class="nav-link active" data-toggle="tab" href="#panel7" role="tab" id="btn-tab-login"><i class="fa fa-user mr-1"></i> Login</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" data-toggle="tab" href="#panel8" role="tab" id="btn-tab-register"><i class="fa fa-user-plus mr-1"></i> Register</a>
					</li>
				</ul>
				<div class="tab-content p-0">
					<div class="tab-pane fade in show active" id="panel7" role="tabpanel">
						<div class="modal-body mb-1">
							<form id="login-form">
								<div class="md-form form-sm">
									<i class="fa fa-user prefix"></i>
									<input type="text" id="login-username" name="username" class="form-control">
									<label for="login-username">Username</label>
								</div>
								<div class="md-form form-sm">
									<i class="fa fa-lock prefix"></i>
									<input type="password" id="login-password" name="password" class="form-control">
									<label for="login-password">Password</label>
								</div>
								<div class="text-center mt-2">
									<button class="btn btn-info" type="submit" id="btn-submit-login">Log in <i class="fa fa-sign-in ml-1"></i></button>
								</div>
							</form>
							<form id="forgot-password-form" class="hidden-xl-up">
								<div class="md-form form-sm">
									<i class="fa fa-user prefix"></i>
									<input type="text" id="forgot-username" name="username" class="form-control">
									<label for="forgot-username">Username</label>
								</div>
								<div class="md-form form-sm">
									<i class="fa fa-lock prefix"></i>
									<input type="password" id="forgot-old-password" name="oldPassword" class="form-control">
									<label for="forgot-old-password">Old Password</label>
								</div>
								<div class="md-form form-sm">
									<i class="fa fa-lock prefix"></i>
									<input type="password" id="forgot-new-password" name="newPassword" class="form-control">
									<label for="forgot-new-password">New Password</label>
								</div>
								<div class="text-center form-sm mt-2">
									<button class="btn btn-info" type="submit" id="btn-submit-reset">Reset Password <i class="fa fa-send ml-1"></i></button>
								</div>
							</form>
						</div>
						<div class="modal-footer display-footer">
							<div class="options text-center text-md-right mt-1 flex-center">
								<p>Not a member? <a href="#" class="blue-text" id="btn-go-register">Sign Up</a></p>
								<!--<p id="text-forgot-password">Forgot <a href="#" class="blue-text" id="btn-forgot-password">Password?</a></p>-->
								<p id="text-back-to-login" class="hidden-xl-up">Back to <a href="#" class="blue-text" id="btn-back-to-login">Login</a></p>
							</div>
							<button type="button" class="btn btn-outline-info waves-effect ml-auto" data-dismiss="modal">Close</button>
						</div>
					</div>
					<div class="tab-pane fade" id="panel8" role="tabpanel">
						<div class="modal-body">
							<form id="register-form">
								<div class="md-form form-sm">
									<i class="fa fa-user prefix"></i>
									<input type="password" id="register-name" name="name" class="form-control">
									<label for="register-name">Full Name</label>
								</div>
								<div class="md-form form-sm">
									<i class="fa fa-user prefix"></i>
									<input type="text" id="register-username" name="username" class="form-control">
									<label for="register-username">Username</label>
								</div>
								<div class="md-form form-sm">
									<i class="fa fa-envelope prefix"></i>
									<input type="text" id="register-email" name="email" class="form-control">
									<label for="register-email">Email</label>
								</div>
								<div class="md-form form-sm">
									<i class="fa fa-lock prefix"></i>
									<input type="password" id="register-password" name="password" class="form-control">
									<label for="register-password">Password</label>
								</div>
								<div class="text-center form-sm mt-2">
									<button class="btn btn-info" type="submit" id="btn-submit-register">Sign up <i class="fa fa-sign-in ml-1"></i></button>
								</div>
							</form>
						</div>
						<div class="modal-footer">
							<div class="options text-right flex-center">
								<p class="pt-1">Already have an account? <a href="#" class="blue-text" id="btn-go-login">Log In</a></p>
							</div>
							<button type="button" class="btn btn-outline-info waves-effect ml-auto" data-dismiss="modal">Close</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>