<?php

if (!isset($_SESSION))
	session_start();

if (isset($_SESSION['userid']) && isset($_SESSION['token']) && isset($_SESSION['name']) && isset($_SESSION['type']))
{
	echo '<span id="user-data" class="hidden-xl-up" data-userid="'.$_SESSION['userid'].'" data-token="'.$_SESSION['token'].'" data-name="'.$_SESSION['name'].'" data-type="'.$_SESSION['type'].'"></span>';
}

?>

<footer class="page-footer center-on-small-only mt-0 mdb-color darken-3">
	<div class="container-fluid">
		<div class="row " data-wow-delay="0.2s">
			<div class="col-md-12 text-center mb-3 mt-3">
				<i class="fa fa-mortar-board fa-4x orange-text"></i>
				<h2 class="mt-3 mb-3">Join Us</h2>
				<p class="white-text mb-5">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
				<a href="#" class="btn btn-warning" data-toggle="modal" data-target="#login-modal">LOGIN</a>
				<a href="#" class="btn btn-info" data-toggle="modal" data-target="#login-modal">REGISTER</a>
			</div>
			<hr class="w-100 mt-4 mb-5">
		</div>
	</div>

	<div class="footer-copyright">
		<div class="container-fluid">
			© 2017 Copyright: <a href="http://www.MDBootstrap.com"> WebDroid Solutions </a>
		</div>
	</div>
</footer>