<?php

if (!isset($_SESSION))
	session_start();

unset($_SESSION['userid']);
unset($_SESSION['token']);
unset($_SESSION['name']);
unset($_SESSION['type']);
header('location: index.php');
die();

?>