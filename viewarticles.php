<?php

if (!isset($_SESSION))
	session_start();

if (!isset($_SESSION['userid']) || !isset($_SESSION['token']) || !isset($_SESSION['type']))
{
	header('location: index.php');
}

require_once 'cfg.php';

$Response=file_get_contents($BaseUrl.'/getArticlesList?token='.$_SESSION['token']);
$Response = json_decode($Response,true);
if ($Response['responseHeaderDto']['statusCode'] != 0)
{
	header('location: search.php');
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta http-equiv="x-ua-compatible" content="ie=edge">

	<title>Journal Website Design</title>

	<link rel="stylesheet" href="css/font-awesome.min.css">
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<link href="css/mdb.min.css" rel="stylesheet">
	<link href="css/style.css" rel="stylesheet">
	<style>
	@media (max-width: 740px) {
		.full-height,
		.full-height body,
		.full-height header,
		.full-height header .view {
			height: 700px; 
		}
	}
</style>
</head>
<body class="university">	
	<header>
		<?php include 'nav.php'; ?>

		<div id="home" class="view hm-black-strong-1 jarallax" data-jarallax='{"speed": 0.2}' style="background-image: url('images/bg-viewarticle.jpg'); height: 525px; min-height: 500px">
			<div class="full-bg-img" style="height: 525px">
				<div class="container flex-center">
					<div class="row smooth-scroll">
						<div class="col-md-12 white-text text-center">
							<div class="wow1 fadeInDown" data-wow1-delay="0.2s">
								<h2 class="display-3 font-bold mb-2">UBIT Journal</h2>
								<hr class="hr-light">
								<h3 class="subtext-header mt-4 mb-5">View Articles</h3>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<?php include_once 'login_register_modal.php' ?>
	</header>

	<main class="grey lighten-3">
		<div class="container py-5">
			<div class="divider-new mb-0 mt-3 pb-3">
				<h2 class="text-center font-up font-bold wow1 fadeIn">View Articles</h2>
			</div>
			<div class="container">
				<div class="table">
					<!--Table-->
					<table class="table">
						<thead class="mdb-color darken-3">
							<tr class="text-white">
								<th>#</th>
								<th>Date</th>
								<th>Title</th>
								<?php
								if ($_SESSION['type'] == 'admin')
									echo '<th>Username</th>';
								?>
								<th>Status</th>
								<th style="width: 60px">Action</th>
							</tr>
						</thead>
						<tbody>
							<?php
							if ($Response['responseHeaderDto']['statusCode'] == 0)
							{
								foreach ($Response['articleDtos'] as $Key => $Article)
								{

									?>
									<tr>
										<th scope="row"><?php echo ($Key + 1) ?></th>
										<td><?php echo date('d M, Y @ h:i A', strtotime($Article['createdDate'])) ?></td>
										<td><?php echo $Article['title'] ?></td>
										<?php
										if ($_SESSION['type'] == 'admin')
											echo '<td>'.$Article['user']['username'].'</td>';

										$Status = null;
										switch ($Article['status']) {
											case 0:
											$Status = "Pending";
											break;
											case 1:
											$Status = "Approved";
											break;
											case 2:
											$Status = "Rejected";
											break;
											case 3:
											$Status = "Published";
											break;
										}
										?>
										<td><?php echo $Status ?></td>
										<td>
											<a class="btn-floating btn-sm teal darken-2" href="editarticle.php?id=<?php echo $Article['id'] ?>"><i class="fa fa-pencil"></i></a>
										</td>
									</tr>
									<?php
								}
							}
							?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</main>

	<?php include 'footer.php'; ?>
	<script type="text/javascript" src="js/jquery.min.js"></script>
	<script type="text/javascript" src="js/popper.min.js"></script>
	<script type="text/javascript" src="js/bootstrap.min.js"></script>
	<script type="text/javascript" src="js/mdb.min.js"></script>
	<script type="text/javascript">
		$('.dropdown-toggle').dropdown();
	</script>
</body>
</html>