<?php

if (!isset($_SESSION))
	session_start();

if (!isset($_GET['id']) || empty($_GET['id']))
{
	header('location: search.php');
}

require_once 'cfg.php';

$Response=file_get_contents($BaseUrl.'/getJournalById?journalId='.$_GET['id']);
$Response = json_decode($Response,true);
if ($Response['responseHeaderDto']['statusCode'] != 0)
{
	header('location: search.php');
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta http-equiv="x-ua-compatible" content="ie=edge">

	<title>Journal Website Design</title>

	<link rel="stylesheet" href="css/font-awesome.min.css">
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<link href="css/mdb.min.css" rel="stylesheet">
	<link href="css/style.css" rel="stylesheet">
	<style>
		@media (max-width: 740px) {
			.full-height,
			.full-height body,
			.full-height header,
			.full-height header .view {
				height: 700px; 
			}
		}
	</style>
</head>
<body class="university">	
	<header>
		<?php include 'nav.php'; ?>

		<div id="home" class="view hm-black-strong-1 jarallax" data-jarallax='{"speed": 0.2}' style="background-image: url('images/bg-article.jpg'); height: 525px; min-height: 500px">
			<div class="full-bg-img" style="height: 525px">
				<div class="container flex-center">
					<div class="row smooth-scroll">
						<div class="col-md-12 white-text text-center">
							<div class="wow1 fadeInDown" data-wow1-delay="0.2s">
								<h2 class="display-3 font-bold mb-2">UBIT Journal</h2>
								<hr class="hr-light">
								<h3 class="subtext-header mt-4 mb-5"><?php echo $Response['journalDto']['name'] ?></h3>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<?php include_once 'login_register_modal.php' ?>
	</header>

	<main class="grey lighten-3">
		<div class="container py-5">
			<div class="mt-1">
				<div class="row pb-2 pt-1 wow1 fadeIn" data-wow1-delay="0.4s">
					<div class="col-3 col-xs-6 col-sm-6 col-md-3">
						<div>
							<img src="<?php echo $Response['journalDto']['image']['url'] ?>" class="img-fluid z-depth-2" alt="<?php echo $Response['journalDto']['name'] ?>">
						</div>
					</div>
					<div class="col-9 col-xs-6 col-sm-6 col-md-9 mb-2">
						<h6 class="font-up mb-3"><div class="text-muted grey-text font-bold">| <?php echo date('F d, Y', strtotime($Response['journalDto']['date'])) ?></div></h6>
						<h4 class="teal-text pb-3"><?php echo $Response['journalDto']['name'] ?></h4>
						<p class="grey-text-3 font-thin mb-0"><b>Published by:</b> <?php echo $Response['journalDto']['publisher'] ?></p>
						<p class="grey-text-3 font-thin"><b>Views:</b> <?php echo $Response['journalDto']['numberOfViews'] ?></p>
					</div>
					<div class="col-md-12">
						<h5 class="teal-text mt-5">Abstract</h5>
						<p class="grey-text-3 font-thin small" align="justify"><?php echo $Response['journalDto']['abstract'] ?></p>
					</div>
				</div>

				<div class="text-center">
					<a href="<?php echo $Response['journalDto']['journal']['url'] ?>" class="btn btn-lg btn-default active">
						<i class="fa fa-download mr-3"></i> <span>Download Edition (PDF)</span>
					</a>
				</div>
			</div>
		</div>
	</main>

	<?php include 'footer.php'; ?>
	<script type="text/javascript" src="js/jquery.min.js"></script>
	<script type="text/javascript" src="js/popper.min.js"></script>
	<script type="text/javascript" src="js/tether.min.js"></script>
	<script type="text/javascript" src="js/bootstrap.min.js"></script>
	<script type="text/javascript" src="js/mdb.min.js"></script>
	<script type="text/javascript" src="js/scripts.js"></script>
	<script type="text/javascript">
		$('.dropdown-toggle').dropdown();
	</script>
</body>
</html>