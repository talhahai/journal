<?php

if (!isset($_SESSION))
	session_start();

if (!isset($_SESSION['userid']) || !isset($_SESSION['token']) || !isset($_SESSION['type']))
{
	header('location: index.php');
}

if (!isset($_GET['id']) || empty($_GET['id']))
{
	header('location: viewarticles.php');
}

require_once 'cfg.php';

$Response=file_get_contents($BaseUrl.'/getArticleById?articleId='.$_GET['id'].'&token='.$_SESSION['token']);
$Response = json_decode($Response,true);
if ($Response['responseHeaderDto']['statusCode'] != 0)
{
	header('location: search.php');
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta http-equiv="x-ua-compatible" content="ie=edge">

	<title>Journal Website Design</title>

	<link rel="stylesheet" href="css/font-awesome.min.css">
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<link href="css/mdb.min.css" rel="stylesheet">
	<link href="css/style.css" rel="stylesheet">
	<style>
	@media (max-width: 740px) {
		.full-height,
		.full-height body,
		.full-height header,
		.full-height header .view {
			height: 700px; 
		}
	}
</style>
</head>
<body class="university">	
	<header>
		<?php include 'nav.php'; ?>

		<div id="home" class="view hm-black-strong-1 jarallax" data-jarallax='{"speed": 0.2}' style="background-image: url('images/bg-upload.jpg'); height: 525px; min-height: 500px">
			<div class="full-bg-img" style="height: 525px">
				<div class="container flex-center">
					<div class="row smooth-scroll">
						<div class="col-md-12 white-text text-center">
							<div class="wow1 fadeInDown" data-wow1-delay="0.2s">
								<h2 class="display-3 font-bold mb-2">UBIT Journal</h2>
								<hr class="hr-light">
								<h3 class="subtext-header mt-4 mb-5">Edit Article</h3>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<?php include_once 'login_register_modal.php' ?>
	</header>

	<main class="grey lighten-3">
		<div class="container py-5">
			<div class="mt-1">
				<div class="divider-new mb-0 mt-3 pb-3">
					<h2 class="text-center font-up font-bold wow1 fadeIn">EDIT ARTICLE</h2>
				</div>
				<div class="card">
					<div class="card-body px-5 pt-5">
						<form id="edit-article-form" class="form-horizontal" novalidate>
							<input type="hidden" name="articleId" value="<?php echo $_GET['id'] ?>">
							<div class="md-form">
								<input type="text" id="title" name="title" class="form-control" placeholder="Title of your article" value="<?php echo $Response['articleDto']['title'] ?>" required>
								<label><b>Title</b></label>
							</div>

							<div class="md-form pb-4">
								<label class="mb-0"><b>Content</b></label>
							</div>
							<div class="md-form">
								<textarea name="content" required><?php echo $Response['articleDto']['content'] ?></textarea>
							</div>
							
							<?php
							if ($_SESSION['type'] == 'admin')
							{
								?>
								<div class="md-form">
									<select id="status" name="status" class="mdb-select" required>
										<option value="0" <?php echo ($Response['articleDto']['status'] == 0 ? 'selected' : '') ?>>Pending</option>
										<option value="1" <?php echo ($Response['articleDto']['status'] == 1 ? 'selected' : '') ?>>Approved</option>
										<option value="2" <?php echo ($Response['articleDto']['status'] == 2 ? 'selected' : '') ?>>Rejected</option>
										<option value="3" <?php echo ($Response['articleDto']['status'] == 3 ? 'selected' : '') ?>>Published</option>
										<option value="4" <?php echo ($Response['articleDto']['status'] == 4 ? 'selected' : '') ?>>Deleted</option>
									</select>
									<label><b>Status</b></label>
								</div>
								<?php
							}
							?>

							<div class="text-center">
								<button type="submit" id="btn-submit-edit-article-form" class="btn btn-lg btn-default active btn-rounded z-depth-1a"><i class="fa fa-send mr-2" aria-hidden="true"></i> Update article</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</main>

	<?php include 'footer.php'; ?>
	<script type="text/javascript" src="js/jquery.min.js"></script>
	<script type="text/javascript" src="js/popper.min.js"></script>
	<script type="text/javascript" src="js/bootstrap.min.js"></script>
	<script type="text/javascript" src="js/mdb.min.js"></script>
	<script type="text/javascript" src="js/tinymce.min.js"></script>
	<script type="text/javascript" src="js/scripts.js"></script>
	<script>
		$('.mdb-select').material_select();
		tinymce.init({
			selector: 'textarea',
			height: 500,
			theme: 'modern',
			plugins: 'fullpage powerpaste searchreplace autolink advcode fullscreen image link media codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists textcolor wordcount a11ychecker imagetools mediaembed contextmenu colorpicker textpattern',
			toolbar1: 'formatselect | fontselect, fontsizeselect, removeformat | cut, copy, paste | undo, redo',
			toolbar2: '| bold italic underline strikethrough, subscript, superscript | forecolor backcolor | link | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent',
			image_advtab: true,
			content_css: [
			'//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
			'../css/tinymce.min.css'
			]
		});
	</script>
</body>
</html>