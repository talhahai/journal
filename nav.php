<?php

if (!isset($_SESSION))
	session_start();

require_once 'cfg.php';

$ScriptName = end(explode('/', $_SERVER['SCRIPT_NAME']));
$ScriptName = $ScriptName != 'index.php' ? 'index.php' : '';

?>
<nav class="navbar navbar-expand-lg navbar-dark fixed-top scrolling-navbar ">
	<div class="container">
		<a class="navbar-brand" href="index.php"><strong>UBIT Journal</strong></a>
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>
		<div class="collapse navbar-collapse" id="navbarSupportedContent">
			<!--Links-->
			<ul class="navbar-nav mr-auto smooth-scroll">
				<li class="nav-item section-nav-item">
					<a class="nav-link" href="<?php echo $ScriptName ?>#home">Home <span class="sr-only">(current)</span></a>
				</li>
				<?php
				if (isset($_SESSION['userid']) && isset($_SESSION['token']) && isset($_SESSION['name']))
				{
					?>
					<li class="nav-item section-nav-item">
						<a class="nav-link" href="<?php echo $ScriptName ?>#latest" data-offset="100">Latest Editions</a>
					</li>
					<?php
				}
				else
				{
					?>
					<li class="nav-item section-nav-item">
						<a class="nav-link" href="<?php echo $ScriptName ?>#about" data-offset="100">About</a>
					</li>
					<li class="nav-item section-nav-item">
						<a class="nav-link" href="<?php echo $ScriptName ?>#latest" data-offset="100">Latest Editions</a>
					</li>
					<li class="nav-item section-nav-item">
						<a class="nav-link" href="<?php echo $ScriptName ?>#subjects" data-offset="100">Subjects</a>
					</li>
					<li class="nav-item section-nav-item">
						<a class="nav-link" href="<?php echo $ScriptName ?>#process" data-offset="100">Process</a>
					</li>
					<?php
				}
				?>
				<li class="nav-item">
					<a class="nav-link" href="search.php">Search Editions</a>
				</li>
				<?php
				if (isset($_SESSION['userid']) && isset($_SESSION['token']) && isset($_SESSION['name']) && isset($_SESSION['type']))
				{ 
					?>
					<li class="nav-item">
						<a class="nav-link" href="uploadarticle.php">Write Article</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="viewarticles.php">View Articles</a>
					</li>
					<?php 
					if ($_SESSION['type'] == 'admin')
					{
						?>
						<li class="nav-item">
							<a class="nav-link" href="uploadjournal.php">Upload Journal</a>
						</li>
						<?php
					}
				}
				?>
			</ul>
			<ul class="navbar-nav nav-flex-icons">
				<li class="nav-item">
					<?php
					if (isset($_SESSION['userid']) && isset($_SESSION['token']) && isset($_SESSION['name']))
					{
						echo '<a class="nav-link" id="btn-logout">Logout</a>';
					}
					else
						echo '<a class="nav-link" data-toggle="modal" data-target="#login-modal">Login / Register</a>';
					?>
				</li>
			</ul>
		</div>
	</div>
</nav>