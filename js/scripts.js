$(document).ready(function () {
	var BaseUrl = 'http://18.217.17.151/journal/1';
	
	$("#login-modal .modal-dialog").one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function() {
		$("#login-modal .modal-dialog").removeClass('animated shake');
	});
	
	$("#login-form").submit(function(e) {
		var formData = new FormData(this);
		e.preventDefault();
		$("#btn-submit-login").prop('disabled', 'disabled')
		$("#btn-submit-login").html('Logging in <i class="fa fa-spinner fa-spin ml-1"></i>');
		$.ajax({
			type: "post",
			url: BaseUrl+"/login",
			data: formData,
			cache: false,
			contentType: false,
			processData: false,
			success: function (data) {
				if (data['responseHeaderDto']['statusCode'] == 0)
				{
					window.location.href = 'setsession.php?userid='+data['userDto']['id']+'&token='+data['userDto']['token']+'&type='+data['userDto']['type']+'&name='+data['userDto']['name'];
				}
				else if (data['responseHeaderDto']['statusCode'] == 3)
				{
					$("#login-modal .modal-dialog").addClass('animated shake');
					toastr.error('Invalid username or password', '', {positionClass: 'toast-bottom-left'});
				}
				else
				{
					toastr.error('Something went wrong, try again', '', {positionClass: 'toast-bottom-left'});
				}

				$("#btn-submit-login").prop('disabled', '')
				$("#btn-submit-login").html('Log in <i class="fa fa-sign-in ml-1"></i>');
			},
			error: function (data) {
				toastr.error('Something went wrong, try again', '', {positionClass: 'toast-bottom-left'});
				$("#btn-submit-login").prop('disabled', '')
				$("#btn-submit-login").html('Log in <i class="fa fa-sign-in ml-1"></i>');
			}
		});
	});

	$("#forgot-password-form").submit(function(e) {
		var formData = new FormData(this);
		e.preventDefault();
		$.ajax({
			type: "post",
			url: BaseUrl+"/forgot",
			data: formData,
			cache: false,
			contentType: false,
			processData: false,
			success: function (data) {
				if (data['responseHeaderDto']['statusCode'] == 0)
				{
					window.location.href = 'setsession.php?userid='+data['userDto']['id']+'&token='+data['userDto']['token']+'&type='+data['userDto']['type']+'&name='+data['userDto']['name'];
				}
				else
				{
					toastr.error('Something went wrong, try again', '', {positionClass: 'toast-bottom-left'});
				}
			},
			error: function (data) {
				toastr.error('Something went wrong, try again', '', {positionClass: 'toast-bottom-left'});
			}
		});
	});

	$("#register-form").submit(function(e) {
		var formData = new FormData(this);
		e.preventDefault();
		$("#btn-submit-register").prop('disabled', 'disabled')
		$("#btn-submit-register").html('Signing Up <i class="fa fa-spinner fa-spin ml-1"></i>');
		$.ajax({
			type: "post",
			url: BaseUrl+"/signup",
			data: formData,
			cache: false,
			contentType: false,
			processData: false,
			success: function (data) {
				if (data['responseHeaderDto']['statusCode'] == 0)
				{
					window.location.href = 'setsession.php?userid='+data['userDto']['id']+'&token='+data['userDto']['token']+'&type='+data['userDto']['type']+'&name='+data['userDto']['name'];
				}
				else if (data['responseHeaderDto']['statusCode'] == 4)
				{
					$("#login-modal .modal-dialog").addClass('animated shake');
					toastr.error('Username already taken, try different username', '', {positionClass: 'toast-bottom-left'});
				}
				else
				{
					toastr.error('Something went wrong, try again', '', {positionClass: 'toast-bottom-left'});
				}
				
				$("#btn-submit-register").prop('disabled', '')
				$("#btn-submit-register").html('Sign up <i class="fa fa-sign-in ml-1"></i>');
			},
			error: function (data) {
				toastr.error('Something went wrong, try again', '', {positionClass: 'toast-bottom-left'});
				$("#btn-submit-register").prop('disabled', '')
				$("#btn-submit-register").html('Sign up <i class="fa fa-sign-in ml-1"></i>');
			}
		});
	});

	$("#btn-logout").click(function(e) {
		e.preventDefault();
		$("#btn-logout").html('Logging Out <i class="fa fa-spinner fa-spin ml-1"></i>');
		$.ajax({
			type: "get",
			url: BaseUrl+"/logout?token="+$('#user-data').data('token'),
			cache: false,
			contentType: false,
			processData: false,
			success: function (data) {
				if (data['statusCode'] == 0 || data['statusCode'] == 5)
				{
					window.location.href = 'unsetsession.php';
				}
				else
				{
					toastr.error('Something went wrong, try again', '', {positionClass: 'toast-bottom-left'});
				}

				$("#btn-logout").html('Logout');
			},
			error: function (data) {
				toastr.error('Something went wrong, try again', '', {positionClass: 'toast-bottom-left'});
				$("#btn-logout").html('Logout');
			}
		});
	});

	$("#btn-forgot-password").click(function(e) {
		$('#forgot-password-form').fadeIn(1000);
		$('#forgot-password-form').removeClass('hidden-xl-up');
		$('#text-forgot-password').addClass('hidden-xl-up');
		$('#login-form').addClass('hidden-xl-up');
		$('#text-back-to-login').fadeOut(1000);
		$('#text-back-to-login').removeClass('hidden-xl-up');
	});

	$("#btn-back-to-login").click(function(e) {
		$('#login-form').fadeIn(1000);
		$('#login-form').removeClass('hidden-xl-up');
		$('#text-back-to-login').addClass('hidden-xl-up');
		$('#forgot-password-form').addClass('hidden-xl-up');
		$('#forgot-password-form').fadeOut(1000);
		$('#text-forgot-password').removeClass('hidden-xl-up');
	});

	$("#btn-go-login").click(function(e) {
		$("#btn-tab-login").trigger('click')
	});

	$("#btn-go-register").click(function(e) {
		$("#btn-tab-register").trigger('click')
	});

	$("#upload-article-form").submit(function(e) {
		tinymce.triggerSave();
		var formData = new FormData(this);
		formData.set('content', formData.get('content').replace('<!DOCTYPE html>', ''));
		$("#btn-submit-upload-article-form").prop('disabled', 'disabled')
		$("#btn-submit-upload-article-form").html('<i class="fa fa-spinner fa-spin mr-2"></i> Uploading Article');
		e.preventDefault();
		$.ajax({
			type: "post",
			url: BaseUrl+"/addArticle?token="+$("#user-data").data('token'),
			data: formData,
			cache: false,
			contentType: false,
			processData: false,
			success: function (data) {
				if (data['responseHeaderDto']['statusCode'] == 0)
				{
					toastr.success('Article uploaded successfully', '', {positionClass: 'toast-bottom-left'});
					$("#upload-article-form")[0].reset();
					tinymce.triggerSave();
				}
				else
				{
					toastr.error('Article not uploaded, try again', '', {positionClass: 'toast-bottom-left'});
				}

				$("#btn-submit-upload-article-form").prop('disabled', '')
				$("#btn-submit-upload-article-form").html('<i class="fa fa-send mr-2" aria-hidden="true"></i> Upload Article');
			},
			error: function (data) {
				toastr.error('Something went wrong, try again', '', {positionClass: 'toast-bottom-left'});
				$("#btn-submit-upload-article-form").prop('disabled', '')
				$("#btn-submit-upload-article-form").html('<i class="fa fa-send mr-2" aria-hidden="true"></i> Upload Article');
			}
		});
	});

	$("#edit-article-form").submit(function(e) {
		tinymce.triggerSave();
		var formData = new FormData(this);
		formData.set('content', formData.get('content').replace('<!DOCTYPE html>', ''));
		$("#btn-submit-edit-article-form").prop('disabled', 'disabled')
		$("#btn-submit-edit-article-form").html('<i class="fa fa-spinner fa-spin mr-2"></i> Updating Article');
		e.preventDefault();
		$.ajax({
			type: "post",
			url: BaseUrl+"/editArticle?token="+$("#user-data").data('token'),
			data: formData,
			cache: false,
			contentType: false,
			processData: false,
			success: function (data) {
				if (data['responseHeaderDto']['statusCode'] == 0)
				{
					toastr.success('Article edited successfully', '', {positionClass: 'toast-bottom-left'});
				}
				else
				{
					toastr.error('Article not edited, try again', '', {positionClass: 'toast-bottom-left'});
				}

				$("#btn-submit-edit-article-form").prop('disabled', '')
				$("#btn-submit-edit-article-form").html('<i class="fa fa-send mr-2" aria-hidden="true"></i> Update Article');
			},
			error: function (data) {
				toastr.error('Something went wrong, try again', '', {positionClass: 'toast-bottom-left'});
				$("#btn-submit-edit-article-form").prop('disabled', '')
				$("#btn-submit-edit-article-form").html('<i class="fa fa-send mr-2" aria-hidden="true"></i> Update Article');
			}
		});
	});

	$("#upload-journal-form").submit(function(e) {
		e.preventDefault();
		$("#btn-submit-upload-journal-form").prop('disabled', 'disabled')
		$("#btn-submit-upload-journal-form").html('<i class="fa fa-spinner fa-spin mr-2"></i> Uploading Journal');
		var formData = new FormData(this);
		if (formData.get('Filedata').size != 0)
		{
			formData.append('type', 'image');
			$.ajax({
				type: "post",
				url: BaseUrl+"/postfile",
				data: formData,
				cache: false,
				contentType: false,
				processData: false,
				success: function (data) {
					if (data['responseHeaderDto']['statusCode'] == 0)
					{
						var coverKey = data['fileDto']['fileKey'];
						formData.append('coverKey', coverKey);
						$.ajax({
							type: "post",
							url: BaseUrl+"/addJournal?token="+$("#user-data").data('token'),
							data: formData,
							cache: false,
							contentType: false,
							processData: false,
							success: function (data) {
								if (data['responseHeaderDto']['statusCode'] == 0)
								{
									toastr.success('Journal uploaded successfully', '', {positionClass: 'toast-bottom-left'});
									$("#upload-journal-form")[0].reset();
								}
								else
								{
									toastr.error('Journal not uploaded, try again', '', {positionClass: 'toast-bottom-left'});
								}

								$("#btn-submit-upload-journal-form").prop('disabled', '')
								$("#btn-submit-upload-journal-form").html('<i class="fa fa-send mr-2" aria-hidden="true"></i> Upload Journal');
							},
							error: function (data) {
								toastr.error('Something went wrong, try again', '', {positionClass: 'toast-bottom-left'});
								$("#btn-submit-upload-journal-form").prop('disabled', '')
								$("#btn-submit-upload-journal-form").html('<i class="fa fa-send mr-2" aria-hidden="true"></i> Upload Journal');
							}
						});
					}
					else
					{
						toastr.error('Journal not uploaded, try again with valid cover image', '', {positionClass: 'toast-bottom-left'});
					}

					$("#btn-submit-upload-journal-form").prop('disabled', '')
					$("#btn-submit-upload-journal-form").html('<i class="fa fa-send mr-2" aria-hidden="true"></i> Upload Journal');
				},
				error: function (data) {
					toastr.error('Something went wrong, try again', '', {positionClass: 'toast-bottom-left'});
					$("#btn-submit-upload-journal-form").prop('disabled', '')
					$("#btn-submit-upload-journal-form").html('<i class="fa fa-send mr-2" aria-hidden="true"></i> Upload Journal');
				}
			});
		}
		else
		{
			$.ajax({
				type: "post",
				url: BaseUrl+"/addJournal?token="+$("#user-data").data('token'),
				data: formData,
				cache: false,
				contentType: false,
				processData: false,
				success: function (data) {
					if (data['responseHeaderDto']['statusCode'] == 0)
					{
						toastr.success('Journal uploaded successfully', '', {positionClass: 'toast-bottom-left'});
						$("#upload-journal-form")[0].reset();
					}
					else
					{
						toastr.error('Journal not uploaded, try again', '', {positionClass: 'toast-bottom-left'});
					}

					$("#btn-submit-upload-journal-form").prop('disabled', '')
					$("#btn-submit-upload-journal-form").html('<i class="fa fa-send mr-2" aria-hidden="true"></i> Upload Journal');
				},
				error: function (data) {
					toastr.error('Something went wrong, try again', '', {positionClass: 'toast-bottom-left'});
					$("#btn-submit-upload-journal-form").prop('disabled', '')
					$("#btn-submit-upload-journal-form").html('<i class="fa fa-send mr-2" aria-hidden="true"></i> Upload Journal');
				}
			});
		}
	});
});