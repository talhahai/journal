<?php

if (!isset($_SESSION))
	session_start();
if (isset($_GET['userid']) && isset($_GET['token']) && isset($_GET['type']))
{
	$_SESSION['userid'] = $_GET['userid'];
	$_SESSION['token'] = $_GET['token'];
	$_SESSION['name'] = $_GET['name'];
	$_SESSION['type'] = $_GET['type'];
	header('location: index.php');
	die();
}

?>